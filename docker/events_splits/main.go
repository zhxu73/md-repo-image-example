package main

// listen for webhook that contains a list of events.
// filter the events, for each good event, make a webhook call containing the single event.

import (
	"context"
	"encoding/json"
	"log"
	"net/url"
	"path"

	"github.com/kelseyhightower/envconfig"
	"github.com/nats-io/nats.go"
)

type Config struct {
	OnlyUpdateEvent  bool   `envconfig:"ONLY_UPDATE_EVENT" default:"false"`
	NatsURL          string `envconfig:"NATS_URL"`
	InputSubject     string `envconfig:"INPUT_SUBJECT" default:"events-splits"`
	InputNested      bool   `envconfig:"INPUT_NESTED" default:"true"`
	InputNestKeyName string `envconfig:"INPUT_NESTED_KEY_NAME" default:"events"`
	OutputSubject    string `envconfig:"OUTPUT_SUBJECT" default:"splitted-event"`
}

type Event struct {
	Event string `json:"event"`
	Path  string `json:"path"`
}

func loadFromEnv() Config {
	var conf Config
	err := envconfig.Process("", &conf)
	if err != nil {
		log.Panic("fail to load config from env", err.Error())
	}

	if conf.NatsURL == "" {
		log.Panic("NatsURL is empty")
	}
	_, err = url.Parse(conf.NatsURL)
	if err != nil {
		log.Panic("WebhookURL is invalid URL", err.Error())
	}
	log.Printf("nats url: %s\n", conf.NatsURL)
	return conf
}

const (
	natsClientName = "events_splits"
	natsQueueName  = "events_splits"
)

func main() {
	conf := loadFromEnv()
	log.Printf("config: %+v", conf)

	ctx, cancel := context.WithCancel(context.Background())
	msgChan := make(chan *nats.Msg, 5)
	nc, err := nats.Connect(conf.NatsURL, nats.Name(natsClientName), nats.ClosedHandler(func(c *nats.Conn) {
		cancel()
		close(msgChan)
		log.Panic("permantely disconnected from NATS")
	}))
	if err != nil {
		log.Fatal("fail to connect to NATS,", err)
	}
	defer nc.Close()
	log.Printf("connected to NATS, %s\n", conf.NatsURL)

	nsub, err := nc.QueueSubscribeSyncWithChan(conf.InputSubject, natsQueueName, msgChan)
	if err != nil {
		log.Panic("fail to subscribe to NATS,", err)
	}
	log.Printf("subscribed to %s\n", conf.InputSubject)
	defer nsub.Drain()

	for msg := range msgChan {
		handleMsg(msg, nc, &conf)
	}
	cancel()
	nsub.Drain()
	nc.Close()
	<-ctx.Done()
}

func handleMsg(msg *nats.Msg, nc *nats.Conn, conf *Config) {
	log.Println("received msg")
	log.Println(string(msg.Data))
	var events []Event
	if conf.InputNested {
		log.Println("checking nested struct")
		var inputPayload map[string]interface{}
		err := json.Unmarshal(msg.Data, &inputPayload)
		if err != nil {
			log.Printf("fail to unmarshal msg data as a nested struct, {\"%s\": [...]}, %s\n", conf.InputNestKeyName, err.Error())
			return
		}
		val, ok := inputPayload[conf.InputNestKeyName]
		if !ok {
			log.Printf("nested key (%s) is missing from events\n", conf.InputNestKeyName)
			return
		}
		if strVal, ok := val.(string); ok {
			log.Println("events value is a string")
			err = json.Unmarshal([]byte(strVal), &events)
			if err != nil {
				log.Println("fail to unmarshal msg data as list of events,", err.Error())
				return
			}
		} else {
			log.Println("events value is NOT a string")
			log.Printf("%+v", val)
			remarshal, err := json.Marshal(val)
			if err != nil {
				log.Printf("fail to re-marshal construct at the nested key (%s), %s\n", conf.InputNestKeyName, err.Error())
				return
			}
			err = json.Unmarshal(remarshal, &events)
			if err != nil {
				log.Println("fail to unmarshal msg data as list of events,", err.Error())
				return
			}
		}
	} else {
		err := json.Unmarshal(msg.Data, &events)
		if err != nil {
			log.Println("fail to unmarshal msg data as list of events,", err.Error())
			return
		}
	}
	log.Printf("got %d events from msg", len(events))
	for _, e := range events {
		if !keepEvent(&e, conf) {
			continue
		}
		marshal, err := json.Marshal(e)
		if err != nil {
			log.Println("fail to marshal 1 event to json,", err.Error())
			continue
		}
		err = nc.Publish(conf.OutputSubject, marshal)
		if err != nil {
			log.Println("fail to publish,", err.Error())
			continue
		}
		log.Println("publish one splited event")
	}
}

func keepEvent(event *Event, conf *Config) bool {
	if conf.OnlyUpdateEvent {
		return event.Event == "modified" && isStatusFile(path.Base(event.Path))
	}
	return isStatusFile(path.Base(event.Path))
}

// old status filename
// const STATUS_FILENAME = "MD_REPO_STATUS.json"

const (
	UnknownStatusFilename    = "SUBMISSION_STATUS.unknown.json"
	InprogressStatusFilename = "SUBMISSION_STATUS.inprogress.json"
	ErroredStatusFilename    = "SUBMISSION_STATUS.errored.json"
	CompletedStatusFilename  = "SUBMISSION_STATUS.completed.json"
)

func isStatusFile(filename string) bool {
	switch filename {
	default:
		return false
	case UnknownStatusFilename:
	case InprogressStatusFilename:
	case ErroredStatusFilename:
	case CompletedStatusFilename:
	}
	return true
}
