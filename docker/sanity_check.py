#!/usr/bin/python3
"""
sanity_check.py <dir>
"""

import argparse
import os

file_extensions = [
    "inp",
    "out",
    "coor",
    "dcd",
    "restart.coor",
    "restart.vel",
    "restart.xsc",
    "vel",
    "xsc",
    "xst",
    "json"
]


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('dir',  type=str,
                        help='directory that contains data files')

    args = parser.parse_args()
    check_file_extensions(args.dir)

    # TODO more checks
    print("sanity check finished successfully")


def check_file_extensions(base_dir: str):
    """
    walk the directory structure to see if all files matches the allowed extensions.
    """
    for root, dirs, files in os.walk(base_dir, topdown=False):
        for name in files:
            print(format("{}", name))
            ext_matched = False
            for ext in file_extensions:
                if name.endswith(ext):
                    ext_matched = True
                    break
            if not ext_matched:
                raise ValueError("file has bad extension, " + name)


if __name__ == "__main__":
    main()

