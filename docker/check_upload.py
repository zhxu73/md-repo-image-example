"""
Check if an upload is ready to be processed by reading the status json file.

pipeline should NOT be kick off if upload is not ready.
"""
import os
import sys
import argparse
import irods
from irods.session import iRODSSession
from irods.collection import iRODSCollection
from typing import Tuple
import json

IRODS_HOST = os.environ.get("IRODS_HOST")
IRODS_PORT = os.environ.get("IRODS_PORT")
IRODS_USER_NAME = os.environ.get("IRODS_USER_NAME")
IRODS_ZONE_NAME = os.environ.get("IRODS_ZONE_NAME")
IRODS_USER_PASSWORD = os.environ.get("IRODS_USER_PASSWORD")

STATUS_FILENAME = "MD_REPO_STATUS.json"

UnknownStatusFilename = "SUBMISSION_STATUS.unknown.json"
InprogressStatusFilename = "SUBMISSION_STATUS.inprogress.json"
ErroredStatusFilename = "SUBMISSION_STATUS.errored.json"
CompletedStatusFilename = "SUBMISSION_STATUS.completed.json"


def main():
    irods_dir = parse_args()
    try:
        session = iRODSSession(host=IRODS_HOST, port=int(
            IRODS_PORT), user=IRODS_USER_NAME, password=IRODS_USER_PASSWORD, zone=IRODS_ZONE_NAME)
        session.users.get(IRODS_USER_NAME)
        session.cleanup()
    except irods.exception.CAT_INVALID_AUTHENTICATION as e:
        print("fail to connect to irods, {}".format(type(e)), file=sys.stderr)
        exit(1)
    except Exception as e:
        print("fail to connect to irods", file=sys.stderr)
        raise e

    with iRODSSession(host=IRODS_HOST, port=int(IRODS_PORT), user=IRODS_USER_NAME, password=IRODS_USER_PASSWORD, zone=IRODS_ZONE_NAME) as session:
        file_content = read_status_file(session, irods_dir)
        print("successfully read status file")
        json_content = json.loads(file_content)
        if json_content["status"] != "completed":
            print("status is not 'completed'", file=sys.stderr)
            exit(1)
        print("success! the upload is ready to be processed")


def parse_args() -> str:
    parser = argparse.ArgumentParser(
        prog="check_upload",
        description="check the status file uploaded to irods")
    parser.add_argument("--irods-dir", type=str, dest="irods_dir",
                        help="irods directory(collection) that contains the data and status json file, mutual exclusive with --status-file-path")
    parser.add_argument("--status-file-path", type=str,
                        dest="status_file_path", help="irods path to the status json file, mutual exclusive with --irods-dir")
    args = parser.parse_args()
    if args.status_file_path:
        _, ext = os.path.splitext(args.status_file_path)
        if ext != ".json":
            raise ValueError(
                "--status-file-path is not path to status json file")
        return os.path.dirname(args.status_file_path)
    return args.irods_dir


def read_status_file(session: iRODSSession, irods_dir: str) -> str:
    status_file_path = os.path.join(irods_dir, CompletedStatusFilename)
    print("status file:", status_file_path)
    try:
        obj = session.data_objects.get(status_file_path)
    except Exception as e:
        print("fail to fetch file, {}, {}".format(
            e, status_file_path), file=sys.stderr)
        raise e
    print("fetched status file, about to read")
    with obj.open("r") as f:
        return f.read()


if __name__ == "__main__":
    main()
