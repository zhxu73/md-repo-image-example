import os
import sys
import argparse
from irods.session import iRODSSession
from irods.collection import iRODSCollection
from typing import Tuple

IRODS_HOST = os.environ.get("IRODS_HOST")
IRODS_PORT = os.environ.get("IRODS_PORT")
IRODS_USER_NAME = os.environ.get("IRODS_USER_NAME")
IRODS_ZONE_NAME = os.environ.get("IRODS_ZONE_NAME")
IRODS_USER_PASSWORD = os.environ.get("IRODS_USER_PASSWORD")


def main():
    irods_dir, output_dir = parse_args()
    with iRODSSession(host=IRODS_HOST, port=int(IRODS_PORT), user=IRODS_USER_NAME, password=IRODS_USER_PASSWORD, zone=IRODS_ZONE_NAME) as session:
        coll = session.collections.get(irods_dir)
        download_files(session, coll, output_dir)


def parse_args() -> Tuple[str, str]:
    parser = argparse.ArgumentParser(
        prog="download_data",
        description="download data from irods")
    parser.add_argument("output_dir", type=str)
    parser.add_argument("--irods-dir", type=str, dest="irods_dir",
                        help="irods directory(collection) that contains the data and status json file, mutual exclusive with --status-file-path")
    parser.add_argument("--status-file-path", type=str,
                        dest="status_file_path", help="irods path to the status json file, mutual exclusive with --irods-dir")
    args = parser.parse_args()
    if args.status_file_path:
        _, ext = os.path.splitext(args.status_file_path)
        if ext != ".json":
            raise ValueError(
                "--status-file-path is not path to status json file")
        return os.path.dirname(args.status_file_path), args.output_dir
    return args.irods_dir, args.output_dir


def download_files(session: iRODSSession, base_coll: iRODSCollection, output_dir: str):
    """
    TODO do we want to go into sub-directories (recursive)?
    """
    for col in base_coll.subcollections:
        print(col)
    for obj in base_coll.data_objects:
        print(obj)
        local_path = os.path.join(output_dir, os.path.basename(obj.path))
        session.data_objects.get(obj.path, local_path)


if __name__ == "__main__":
    main()
